package com.geekhub.task1;

import java.lang.reflect.Field;

public class BeanRepresenter {

    public <E> String represent(E value) throws IllegalAccessException {
        Field[] fields = value.getClass().getDeclaredFields();
        StringBuilder builder = new StringBuilder();
        builder.append(value.getClass().getSimpleName()).append("\n");
        for (Field field : fields) {
            field.setAccessible(true);
            builder.append(field.getName()).append(" ");
            builder.append(field.get(value)).append("\n");
        }

        return builder.toString();
    }
}
