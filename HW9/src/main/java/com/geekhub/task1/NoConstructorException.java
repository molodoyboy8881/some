package com.geekhub.task1;

public class NoConstructorException extends Exception {

    public NoConstructorException(String message) {
        super(message);
    }
}
