package com.geekhub.task1;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class Application {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException, NoConstructorException, InvocationTargetException {
        Person p1 = new Person(11,"Joe", List.of("One","Two"));
        Person p2 = new Person(11, "John", List.of("One", "Two"));

        BeanComparator<Person> comparator = new BeanComparator<>();
        System.out.println(comparator.compare(p1,p2));

        BeanRepresenter representer = new BeanRepresenter();
        System.out.println(representer.represent(p1));

        p2 = new Person(21, "Oleg", new ArrayList<>());
        CloneCreator cloneCreator = new CloneCreator();
        Person person = cloneCreator.clone(p2);
        person.getList().add("Three");
        System.out.println(p2);
        System.out.println(person);
    }
}
