package com.geekhub.task1;

import java.lang.reflect.Field;

public class BeanComparator<T> {

    public String compare(T o1, T o2) throws IllegalAccessException {
        Field[] fields1 = o1.getClass().getDeclaredFields();
        Field[] fields2 = o2.getClass().getDeclaredFields();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < fields1.length; ++i) {
            fields1[i].setAccessible(true);
            fields2[i].setAccessible(true);

            builder.append(fields1[i].getName()).append(" ");
            builder.append(fields1[i].get(o1)).append(" ");
            builder.append(fields2[i].get(o2)).append(" ");

            boolean equals = fields1[i].get(o1).equals(fields2[i].get(o2));
            builder.append(equals).append("\n");
        }

        return builder.toString();
    }
}
