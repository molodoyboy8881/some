package com.geekhub.task1;

import java.lang.reflect.*;

public class CloneCreator {

    public <E> E clone(E value) throws IllegalAccessException, InvocationTargetException,
            InstantiationException, NoConstructorException {
        Class<?> clazz = value.getClass();
        if (clazz.isArray()) {
            return cloneArray(value);
        }

        if (clazz.getConstructors().length == 0) {
            throw new NoConstructorException(clazz.getName() + "hasn't public constructor!");
        }

        Constructor<?> constructor = null;
        for (Constructor<?> cont : clazz.getConstructors()) {
            constructor = cont;
            if (constructor.getParameterCount() == 0) {
                break;
            }
        }

        @SuppressWarnings("unchecked")
        E newValue = (E) constructor.newInstance();
        for (Field field : clazz.getDeclaredFields()) {
            if (Modifier.isStatic(field.getModifiers())) {
                continue;
            }

            field.setAccessible(true);
            Object object = field.get(value);
            if (!isSimpleWrappedClass(object.getClass())) {
                object = clone(object);
            }

            field.set(newValue, object);
        }

        return newValue;
    }

    private boolean isSimpleWrappedClass(Class<?> clazz) {
        return clazz == Integer.class || clazz == Long.class ||
                clazz == Float.class || clazz == Double.class ||
                clazz == Boolean.class || clazz == Character.class ||
                clazz == Byte.class || clazz == Short.class || clazz == String.class;
    }

    private  <E> E cloneArray(E value) {
        Class<?> clazz = value.getClass();
        @SuppressWarnings("unchecked")
        E newValue = (E) Array.newInstance(clazz.getComponentType(), Array.getLength(value));
        int length = Array.getLength(value);
        for (int i = 0; i < length; ++i) {
            Object element = Array.get(value, i);
            Array.set(newValue, i, element);
        }

        return newValue;
    }
}
