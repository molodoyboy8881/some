package com.geekhub.task1;

import java.util.List;

class Person {
    private int age;
    private String name;
    private List<String> list;

    public Person() {

    }

    public Person(int age, String name, List<String> list) {
        this.age = age;
        this.name = name;
        this.list = list;
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", list =" + list +
                '}';
    }

    public List<String> getList() {
        return list;
    }
}
