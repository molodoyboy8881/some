package com.geekhub.task2;

import com.geekhub.task2.adapter.UseDataAdapter;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Set;

public class JsonSerializer {
    private static final Set<Class<?>> simpleTypes = Set.of(
            String.class,
            Integer.class,
            Short.class,
            Long.class,
            Byte.class,
            Double.class,
            Float.class,
            Character.class,
            Boolean.class,
            int.class,
            short.class,
            long.class,
            byte.class,
            double.class,
            float.class,
            char.class,
            boolean.class
    );

    public static Object serialize(Object o) throws SerializationException {
        if (null == o) {
            return "null";
        }

        if (simpleTypes.contains(o.getClass())) {
            return o;
        } else {
            try {
                return toJsonObject(o);
            } catch (SerializationException e) {
                throw new SerializationException("Exception during serialization " + o.toString(), e);
            } catch (Exception e) {
                throw new SerializationException("Exception during serialization " + o.toString(), e);
            }
        }
    }

    private static JSONObject toJsonObject(Object o) throws Exception {
        JSONObject jsonObject = new JSONObject();
        for (Field field : o.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(Ignore.class)) {
                continue;
            }

            field.setAccessible(true);
            Object object = field.get(o);
            if (field.isAnnotationPresent(UseDataAdapter.class)) {
                Class<?> clazz = field.getAnnotation(UseDataAdapter.class).value();
                Method method = clazz.getMethod("toJson", Object.class);
                Object instance = clazz.getConstructors()[0].newInstance();
                object = method.invoke(instance, object);
            } else if (!simpleTypes.contains(object.getClass())) {
                object = serialize(object);
            }

            jsonObject.put(field.getName(), object);
        }

        return jsonObject;
    }
}
