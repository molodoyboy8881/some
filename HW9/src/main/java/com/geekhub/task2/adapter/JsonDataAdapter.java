package com.geekhub.task2.adapter;

import com.geekhub.task2.SerializationException;

public interface JsonDataAdapter<T> {
    Object toJson(T o) throws SerializationException;
}

