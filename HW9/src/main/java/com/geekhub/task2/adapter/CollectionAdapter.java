package com.geekhub.task2.adapter;

import com.geekhub.task2.SerializationException;
import com.geekhub.task2.JsonSerializer;
import org.json.JSONArray;
import org.json.JSONException;
import java.util.Collection;

public class CollectionAdapter implements JsonDataAdapter<Collection<?>> {
    @Override
    public Object toJson(Collection<?> c) throws JSONException, SerializationException {
        JSONArray jsonArray = new JSONArray();
        for (Object object : c) {
            Object serializedObject = JsonSerializer.serialize(object);
            jsonArray.put(serializedObject);
        }

        return jsonArray;
    }
}
