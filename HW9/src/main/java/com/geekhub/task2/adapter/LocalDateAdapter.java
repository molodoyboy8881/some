package com.geekhub.task2.adapter;

import java.time.LocalDate;

public class LocalDateAdapter implements JsonDataAdapter<LocalDate> {

    @Override
    public Object toJson(LocalDate date) {
        return date.toString();
    }
}
