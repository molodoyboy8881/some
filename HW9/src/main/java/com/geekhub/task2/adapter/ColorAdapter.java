package com.geekhub.task2.adapter;

import java.awt.*;

public class ColorAdapter implements JsonDataAdapter<Color> {

    @Override
    public Object toJson(Color o) {
        return "(" + o.getRed() + "," + o.getGreen() + "," + o.getBlue() + ")";
    }
}