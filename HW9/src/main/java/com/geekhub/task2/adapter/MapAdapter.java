package com.geekhub.task2.adapter;

import com.geekhub.task2.SerializationException;
import com.geekhub.task2.JsonSerializer;
import org.json.JSONObject;

import java.util.Map;

public class MapAdapter implements JsonDataAdapter<Map<?, ?>> {

    @Override
    public Object toJson(Map<?, ?> map) throws SerializationException {
        JSONObject jsonObject = new JSONObject();
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            String key = JsonSerializer.serialize(entry.getKey()).toString();
            Object value = JsonSerializer.serialize(entry.getValue());
            jsonObject.put(key, value);
        }

        return jsonObject;
    }
}
