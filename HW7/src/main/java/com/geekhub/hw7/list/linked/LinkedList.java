package com.geekhub.hw7.list.linked;

import com.geekhub.hw7.list.List;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

public class LinkedList<E> implements List<E> {
    private Node<E> head;
    private Node<E> tail;
    private int size = 0;

    @Override
    public boolean add(E element) {
        Node<E> node = new Node<>(element, null);
        if (tail == null) {
            head = node;
        } else {
            tail.next = node;
        }

        tail = node;
        size++;

        return true;
    }

    @Override
    public boolean add(int index, E element) {
        if (index < 0 || index > size) {
            return false;
        }

        Node<E> node = new Node<>(element, null);
        if (index == 0) {
            if (tail == null) {
                head = node;
                tail = node;
            } else {
                node.next = head;
                head = node;
            }
        } else if (index == size) {
            tail.next = node;
            tail = node;
        } else {
            Node<E> before = head;
            for (int i = 1; i < index; ++i) {
                before = before.next;
            }

            node.next = before.next;
            before.next = node;
        }

        size += 1;

        return true;
    }

    @Override
    public boolean addAll(List<E> elements) {
        if (elements.size() == 0) {
            return false;
        }

        for (E element : elements) {
            add(element);
        }

        return true;
    }

    @Override
    public boolean addAll(int index, List<E> elements) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("size = " + size + " index = " + index);
        }

        if (elements.isEmpty()) {
            return false;
        }

        if (index == 0) {
            Node<E> start = null;
            Node<E> iterated = head;
            for (E element : elements) {
                if (start == null) {
                    start = new Node<>(element, null);
                    iterated = start;
                } else {
                    iterated.next = new Node<>(element, null);
                    iterated = iterated.next;
                }
                size += 1;
            }

            if (tail == null) {
                tail = iterated;
            }

            iterated.next = head;
            head = start;
        } else {
            Node<E> iterated = head;
            for (int i = 1; i < index; ++i) {
                iterated = iterated.next;
            }

            Node<E> after = iterated.next;
            for (E element : elements) {
                iterated.next = new Node<>(element, null);
                iterated = iterated.next;
                size += 1;
            }

            iterated.next = after;
        }

        return true;
    }

    @Override
    public boolean clear() {
        this.head = this.tail = null;
        this.size = 0;

        return true;
    }

    @Override
    public E remove(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("index = " + index + " size = " + size);
        }

        Node<E> removed = head;
        Node<E> before = null;
        if (index == 0) {
            head = head.next;
        } else {
            for (int i = 1; i < index; ++i) {
                removed = removed.next;
            }

            before = removed;
            removed = before.next;
            before.next = removed.next;
            removed.next = null;
        }

        if (tail == removed) {
            tail = before;
        }
        size -= 1;

        return removed.element;
    }

    @Override
    public E remove(E element) {
        if (size == 0) {
            return null;
        }

        Node<E> removed = head;
        Node<E> before = null;
        if (head.element.equals(element)) {
            head = head.next;
        } else {
            while (!removed.next.element.equals(element)) {
                removed = removed.next;
                if (removed.next == null) {
                    return null;
                }
            }

            before = removed;
            removed = removed.next;
            before.next = removed.next;
            removed.next = null;
        }

        if (tail == removed) {
            tail = before;
        }

        size -= 1;
        return element;
    }

    @Override
    public E get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("index = " + index + " size = " + size);
        }

        Node<E> node = head;
        for (int i = 0; i < index; ++i) {
            node = node.next;
        }

        return node.element;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0 && tail == null;
    }

    @Override
    public int indexOf(E element) {
        int index = 0;
        Iterator<E> it = iterator();
        while (it.hasNext()) {
            if (it.next().equals(element)) {
                return index;
            }

            index += 1;
        }

        return -1;
    }

    @Override
    public boolean contains(E element) {
        return indexOf(element) >= 0;
    }

    @Override
    public Iterator<E> iterator() {
        return new LinkedListIterator<>(head);
    }

    @Override
    public String toString() {
        Iterator<E> it = iterator();
        if (!it.hasNext()) {
            return null;
        }

        StringBuilder str = new StringBuilder("{");
        while (it.hasNext()) {
            E element = it.next();
            str.append(", ").append(element.toString());
        }

        return str.append("}").toString();
    }

    @Override
    public List<E> filter(Predicate<E> predicate) {
        Iterator<E> it = iterator();
        LinkedList<E> filteredList = new LinkedList<>();
        while (it.hasNext()) {
            E element = it.next();
            if (predicate.test(element)) {
                filteredList.add(element);
            }
        }

        return filteredList;
    }

    @Override
    public <T> List<T> map(Function<E, T> mapper) {
        Iterator<E> it = iterator();
        List<T> mappedList = new LinkedList<>();
        while (it.hasNext()) {
            mappedList.add(mapper.apply(it.next()));
        }

        return mappedList;
    }

    @Override
    public Optional<E> max(Comparator<E> comparator) {
        if (size == 0) {
            return Optional.empty();
        }

        Iterator<E> it = iterator();
        E max = it.next();
        while (it.hasNext()) {
            E element = it.next();
            if (comparator.compare(max, element) < 0) {
                max = element;
            }
        }

        return Optional.of(max);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        List<?> list = (List<?>) obj;
        if (list.size() != size) {
            return false;
        }

        Iterator<E> it1 = iterator();
        Iterator<?> it2 = list.iterator();
        while (it1.hasNext()) {
            if (!it1.next().equals(it2.next())) {
                return false;
            }
        }

        return true;
    }
}
