package com.geekhub.hw4.task2;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class TaskManagerImpl implements TaskManager {
    private final Map<LocalDateTime, List<Task>> tasksByDate;

    public TaskManagerImpl() {
        tasksByDate = new TreeMap<>();
    }

    @Override
    public void add(LocalDateTime date, Task task) {
        if (tasksByDate.get(date) == null) {
            tasksByDate.put(date, new ArrayList<>());
        }

        tasksByDate.get(date).add(task);
    }

    @Override
    public void remove(LocalDateTime date) {
        tasksByDate.remove(date);
    }

    @Override
    public Set<String> getCategories() {
        Set<String> categories = new HashSet<>();
        for (List<Task> tasks : tasksByDate.values()) {
            for (Task task : tasks) {
                categories.add(task.getCategory());
            }
        }

        return categories;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories(String... categories) {
        Map<String, List<Task>> tasksByCategories = new LinkedHashMap<>();
        for (String category : categories) {
            tasksByCategories.put(category, getTasksByCategory(category));
        }

        return tasksByCategories;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        List<Task> tasksByCategory = new ArrayList<>();
        for (List<Task> tasks : tasksByDate.values()) {
            for (Task task : tasks) {
                if (task.getCategory().equals(category)) {
                    tasksByCategory.add(task);
                }
            }
        }

        return tasksByCategory;
    }

    @Override
    public List<Task> getTasksForToday() {
        List<Task> tasksForToday = new ArrayList<>();
        LocalDate today = LocalDate.now();
        for (LocalDateTime key : tasksByDate.keySet()) {
            if (today.equals(key.toLocalDate())) {
                tasksForToday.addAll(tasksByDate.get(key));
            }
        }

        return tasksForToday;
    }
}
