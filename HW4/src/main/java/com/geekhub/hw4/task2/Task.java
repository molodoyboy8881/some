package com.geekhub.hw4.task2;

public class Task {
    private final String name;
    private final String category;

    public Task(String name, String category) {
        this.name = name;
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return "Task{" +
                "name='" + name + '\'' +
                ", category='" + category + '\'' +
                '}';
    }
}
