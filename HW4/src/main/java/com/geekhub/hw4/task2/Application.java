package com.geekhub.hw4.task2;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Application {
    public static void main(String[] args) {
        Task task1 = new Task("1", "play football");
        Task task2 = new Task("2", "play basketball");
        Task task3 = new Task("3", "meet friends");
        Task task4 = new Task("4", "watch TV");
        Task task5 = new Task("5", "eat meal");
        Task task6 = new Task("6", "go shopping");
        Task task7 = new Task("7", "watch TV");
        Task task8 = new Task("8", "eat meal");
        Task task9 = new Task("9", "go shopping");

        TaskManagerImpl taskManager = new TaskManagerImpl();
        taskManager.add(LocalDateTime.of(LocalDate.of(2020, 12, 10),
                LocalTime.of(17, 30)), task2);
        taskManager.add(LocalDateTime.of(LocalDate.of(2020, 12, 10),
                LocalTime.of(19, 0)), task3);
        taskManager.add(LocalDateTime.of(LocalDate.of(2020, 12, 11),
                LocalTime.of(11, 0)), task4);
        taskManager.add(LocalDateTime.of(LocalDate.of(2020, 12, 12),
                LocalTime.of(18, 0)), task6);
        taskManager.add(LocalDateTime.of(LocalDate.of(2020, 12, 13),
                LocalTime.of(18, 0)), task8);
        taskManager.add(LocalDateTime.of(LocalDate.of(2020, 10, 14),
                LocalTime.of(18, 0)), task9);

        taskManager.add(LocalDateTime.of(LocalDate.now(), LocalTime.of(16, 30)), task1);
        taskManager.add(LocalDateTime.of(LocalDate.now(), LocalTime.of(21, 1)), task5);
        taskManager.add(LocalDateTime.of(LocalDate.now(), LocalTime.of(21, 1)), task7);

        System.out.println(taskManager.getCategories());
        System.out.println(taskManager.getTasksByCategory("watch TV"));
        System.out.println(taskManager.getTasksByCategories("eat meal", "go shopping"));
        System.out.println(taskManager.getTasksForToday());

        taskManager.remove(LocalDateTime.of(LocalDate.now(), LocalTime.of(16, 30)));
        System.out.println(taskManager.getTasksForToday());
    }
}
