package com.geekhub.hw4.task1.sorters;

import com.geekhub.hw4.task1.direction.SortDirection;

public class BubbleSortArraySorter implements ArraySorter {
    @Override
    public void sort(int[] array, SortDirection direction) {
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1; ++i) {
                if (chooseDirection(direction, array[i], array[i + 1])) {
                    isSorted = false;

                    int swap = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = swap;
                }
            }
        }
    }
}
