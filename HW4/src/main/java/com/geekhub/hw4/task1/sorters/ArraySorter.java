package com.geekhub.hw4.task1.sorters;

import com.geekhub.hw4.task1.direction.SortDirection;

public interface ArraySorter {
    void sort(int[] array, SortDirection direction);

    default boolean chooseDirection(SortDirection direction, int first, int second) {
        if (direction.getDirection() > 0) {
            return first > second;
        }

        return first < second;
    }
}
