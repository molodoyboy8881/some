package com.geekhub.hw4.task1.application;

import com.geekhub.hw4.task1.array.RandomArray;
import com.geekhub.hw4.task1.direction.SortDirection;
import com.geekhub.hw4.task1.sorters.*;

import java.util.Arrays;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        System.out.print("Enter direction(ASC or DSC): ");
        Scanner scanner = new Scanner(System.in);
        String directionValue = scanner.nextLine().toUpperCase();
        SortDirection direction = SortDirection.valueOf(directionValue);

        int[] array1 = new RandomArray(10).createRandomArray();
        new BubbleSortArraySorter().sort(array1, direction);
        System.out.println("Sort result: " + Arrays.toString(array1));

        int[] array2 = new RandomArray(11).createRandomArray();
        new InsertionSortArraySorter().sort(array2, direction);
        System.out.println("Sort result: " + Arrays.toString(array2));

        int[] array3 = new RandomArray(12).createRandomArray();
        new SelectionSortArraySorter().sort(array1, direction);
        System.out.println("Sort result: " + Arrays.toString(array3));
    }
}
