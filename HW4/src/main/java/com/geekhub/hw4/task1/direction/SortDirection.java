package com.geekhub.hw4.task1.direction;

public enum SortDirection {
    ASC(1),
    DSC(-1);

    private final int direction;

    SortDirection(int direction) {
        this.direction = direction;
    }

    public int getDirection() {
        return direction;
    }
}
