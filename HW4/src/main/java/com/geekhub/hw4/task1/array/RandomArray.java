package com.geekhub.hw4.task1.array;

import java.util.Random;

public class RandomArray {
    private final int[] array;

    public RandomArray(int length) {
        this.array = new int[length];
    }

    public int[] createRandomArray() {
        Random random = new Random();
        for (int i = 0; i < array.length; ++i) {
            array[i] = random.nextInt(100);
        }

        return array;
    }
}
