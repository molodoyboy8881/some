package com.geekhub.hw4.task1.sorters;

import com.geekhub.hw4.task1.direction.SortDirection;

public class SelectionSortArraySorter implements ArraySorter {
    @Override
    public void sort(int[] array, SortDirection direction) {
        for (int i = 0; i < array.length; ++i) {
            int min = array[i];
            int minIndex = i;
            for (int j = i + 1; j < array.length; ++j) {
                if (chooseDirection(direction, min, array[j])) {
                    min = array[j];
                    minIndex = j;
                }
            }

            if (i != minIndex) {
                int swap = array[i];
                array[i] = array[minIndex];
                array[minIndex] = swap;
            }
        }
    }
}
