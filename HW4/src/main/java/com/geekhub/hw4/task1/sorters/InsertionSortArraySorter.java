package com.geekhub.hw4.task1.sorters;

import com.geekhub.hw4.task1.direction.SortDirection;

public class InsertionSortArraySorter implements ArraySorter {
    @Override
    public void sort(int[] array, SortDirection direction) {
        for (int i = 0; i < array.length; ++i) {
            int value = array[i];
            int j = i - 1;
            for (; j >= 0; --j) {
                if (chooseDirection(direction, array[j], value)) {
                    array[j + 1] = array[j];
                } else {
                    break;
                }
            }
            array[j + 1] = value;
        }
    }
}
