package com.geekhub.hw2.shapes.shapes;

import com.geekhub.hw2.shapes.abstractions.Shape;

public class Circle implements Shape {
    private final float radius;

    public Circle(float radius) {
        this.radius = radius;
    }

    @Override
    public float calculateArea() {
        return (float)Math.PI * radius * radius;
    }

    @Override
    public float calculatePerimeter() {
        return 2 * (float)Math.PI * radius;
    }
}
