package com.geekhub.hw2.shapes.shape_types;

public enum ShapeTypes {
    CIRCLE(1),
    SQUARE(1),
    RECTANGLE(2),
    TRIANGLE(3);

    ShapeTypes(int numberOfParameters) {
        this.numberOfParameters = numberOfParameters;
    }

    private final int numberOfParameters;

    public int getNumberOfParameters() {
        return numberOfParameters;
    }
}
