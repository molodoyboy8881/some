package com.geekhub.hw2.shapes.shapes;

import com.geekhub.hw2.shapes.abstractions.FourSideShape;

public class Rectangle extends FourSideShape {
    private final float a;
    private final float b;

    public Rectangle(float a, float b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public float calculateArea() {
        return a * b;
    }

    @Override
    public float calculatePerimeter() {
        return 2 * (a + b);
    }

    @Override
    public float calculateDiagonal() {
        return (float) Math.sqrt(a * a + b * b);
    }
}
