package com.geekhub.hw2.shapes.shapes;

import com.geekhub.hw2.shapes.abstractions.Shape;

public class Triangle implements Shape {
    private final float a;
    private final float b;
    private final float c;

    public Triangle(float a, float b, float c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public float calculatePerimeter() {
        return a + b + c;
    }

    @Override
    public float calculateArea() {
        float p = (a + b + c) / 2f;
        return (float) Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }
}
