package com.geekhub.hw2.shapes.shapes;

import com.geekhub.hw2.shapes.abstractions.FourSideShape;

public class Square extends FourSideShape {
    private final float a;

    public Square(float a) {
        this.a = a;
    }

    @Override
    public float calculateArea() {
        return a * a;
    }

    @Override
    public float calculatePerimeter() {
        return 4 * a;
    }

    @Override
    public float calculateDiagonal() {
        return (float) Math.sqrt(2 * a * a);
    }
}

