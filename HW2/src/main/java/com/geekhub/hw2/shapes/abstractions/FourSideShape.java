package com.geekhub.hw2.shapes.abstractions;

public abstract class FourSideShape implements Shape{
    public abstract float calculateDiagonal();
}
