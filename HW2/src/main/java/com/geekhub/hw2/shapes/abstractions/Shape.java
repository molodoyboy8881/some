package com.geekhub.hw2.shapes.abstractions;

public interface Shape {
    float calculateArea();

    float calculatePerimeter();
}
