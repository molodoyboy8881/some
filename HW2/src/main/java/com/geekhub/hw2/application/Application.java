package com.geekhub.hw2.application;

import com.geekhub.hw2.shapes.abstractions.FourSideShape;
import com.geekhub.hw2.shapes.shape_types.ShapeTypes;
import com.geekhub.hw2.shapes.abstractions.Shape;
import com.geekhub.hw2.shapes.shapes.Circle;
import com.geekhub.hw2.shapes.shapes.Rectangle;
import com.geekhub.hw2.shapes.shapes.Square;
import com.geekhub.hw2.shapes.shapes.Triangle;

import java.util.Arrays;
import java.util.Scanner;

public class Application {
    private ShapeTypes chooseShape(Scanner scanner) {
        boolean correct;
        ShapeTypes shapeType = null;

        do {
            System.out.print("Choose shape from list: " + Arrays.toString(ShapeTypes.values()) + ": ");

            String shapeName = scanner.nextLine().toUpperCase();
            try {
                shapeType = ShapeTypes.valueOf(shapeName);
                correct = true;
            } catch (IllegalArgumentException e) {
                System.out.println("Enter correct value!");
                correct = false;
            }
        } while (!correct);

        return shapeType;
    }

    private float[] initParameters(Scanner scanner, int numberOfParameters) {
        float[] parameters = new float[numberOfParameters];

        for (int i = 0; i < parameters.length; ) {
            System.out.print("Enter " + (i + 1) + "'st parameter: ");

            String value = scanner.nextLine();
            if (isParameterCorrect(value)) {
                parameters[i] = Float.parseFloat(value);
                ++i;
            } else {
                System.out.print("Enter correct parameter! ");
            }
        }

        return parameters;
    }

    private Shape createShape(ShapeTypes shapeTypes, float[] parameters) {
        switch (shapeTypes) {
            case CIRCLE:
                return new Circle(parameters[0]);
            case SQUARE:
                return new Square(parameters[0]);
            case RECTANGLE:
                return new Rectangle(parameters[0], parameters[1]);
            case TRIANGLE:
                return new Triangle(parameters[0], parameters[1], parameters[2]);
        }

        return null;
    }

    private boolean isParameterCorrect(String value) {
        try {
            if (Float.parseFloat(value) <= 0) {
                return false;
            }
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

    private Triangle initInnerTriangle(FourSideShape shape, float[] parameters) {
        if (shape instanceof Square) {
            return new Triangle(parameters[0], parameters[0], shape.calculateDiagonal());
        } else if (shape instanceof Rectangle) {
            return new Triangle(parameters[0], parameters[1], shape.calculateDiagonal());
        }

        return null;
    }

    private void printResult(ShapeTypes type, Shape shape, float[] parameters) {
        System.out.println(type + "'s area = " + shape.calculateArea());
        System.out.println(type + "'s perimeter = " + shape.calculatePerimeter());

        if (shape instanceof FourSideShape) {
            Triangle triangle = initInnerTriangle((FourSideShape) shape, parameters);

            System.out.print("In " + type + " is Triangle with area = " + triangle.calculateArea());
            System.out.println(" and perimeter = " + triangle.calculatePerimeter());
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Application startApplication = new Application();

        ShapeTypes type = startApplication.chooseShape(scanner);
        float[] parameters = startApplication.initParameters(scanner, type.getNumberOfParameters());
        Shape shape = startApplication.createShape(type, parameters);
        startApplication.printResult(type, shape, parameters);
    }
}
