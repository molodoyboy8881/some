package com.geekhub.hw8.variant2.zip;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipOutputStream;

public class FilesZipper {
    private final List<File> files;
    private final File zipDirectory;
    private final Set<String> zippedFileNames;

    public FilesZipper(List<File> files, File zipDirectory) {
        this.files = files;
        this.zipDirectory = zipDirectory;
        zippedFileNames = new HashSet<>();
    }

    public void zip() throws IOException {
        try(ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipDirectory))) {
            for (File file : files) {
                ZipEntry entry = new ZipEntry(file.getName());
                try(FileInputStream fis = new FileInputStream(file)) {
                    try {
                        zos.putNextEntry(entry);
                    } catch (ZipException e) {
                        String newFileName = createNewFileName(file.getName());
                        entry = new ZipEntry(newFileName);
                        zos.putNextEntry(entry);
                    }

                    int length;
                    byte[] buffer = new byte[1024];
                    while ((length = fis.read(buffer)) >= 0) {
                        zos.write(buffer, 0, length);
                    }

                    zos.closeEntry();
                }
            }

        }
    }

    private String createNewFileName(String fileName) {
        int pointIndex = fileName.lastIndexOf(".");
        String name = fileName.substring(0, pointIndex);
        String extension = fileName.substring(pointIndex);

        File file;
        String newName;
        int counter = 1;
        do {
            newName = name + "(" + counter + ")" + extension;
            file = new File(newName);
            counter++;
        } while (file.exists() || !zippedFileNames.add(newName));

        return newName;
    }
}
