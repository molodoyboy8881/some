package com.geekhub.hw8.variant2.zip;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FilesByTypeFinder {
    private final File rootDirectory;
    private final List<FileType> fileTypes;
    private final Map<FileType, List<File>> filesByType;

    public FilesByTypeFinder(File rootDirectory, List<FileType> fileTypes) {
        this.rootDirectory = rootDirectory;
        this.fileTypes = fileTypes;
        this.filesByType = new HashMap<>();
    }

    public Map<FileType, List<File>> getFoundFiles() {
        findFiles(rootDirectory);
        return filesByType;
    }

    private void findFiles(File rootFile) {
        if (rootFile.isDirectory()) {
            File[] subFiles = rootFile.listFiles();
            if (subFiles != null) {
                for (File file : subFiles) {
                    findFiles(file);
                }
            }
            return;
        }

        FileType type = getFileType(rootFile);
        if (type != FileType.OTHER) {
            List<File> files = filesByType.get(type);
            if (files == null) {
                files = new ArrayList<>();
                filesByType.put(type, files);
            }

            files.add(rootFile);
        }
    }

    private FileType getFileType(File file) {
        int pointIndex = file.getName().lastIndexOf(".");
        if (pointIndex == -1 || pointIndex == 0) {
            return FileType.OTHER;
        }

        String extension = file.getName().substring(pointIndex + 1);
        for (FileType type : fileTypes) {
            if (type.getExtensions().contains(extension)) {
                return type;
            }
        }

        return FileType.OTHER;
    }
}
