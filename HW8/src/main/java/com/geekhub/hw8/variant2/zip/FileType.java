package com.geekhub.hw8.variant2.zip;

import java.util.Collections;
import java.util.Set;

public enum FileType {
    AUDIO(Set.of("mp3", "wav", "wma")),
    VIDEO(Set.of("avi", "mp4", "flv")),
    IMAGE(Set.of("jpeg", "png", "gif", "jpg")),
    OTHER(Collections.emptySet());

    private final Set<String> extensions;

    FileType(Set<String> extensions) {
        this.extensions = extensions;
    }

    public Set<String> getExtensions() {
        return extensions;
    }
}