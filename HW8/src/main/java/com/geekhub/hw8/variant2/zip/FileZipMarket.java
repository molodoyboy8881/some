package com.geekhub.hw8.variant2.zip;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class FileZipMarket {
    private final File rootDirectory;
    private final List<FileType> fileTypes;

    public FileZipMarket(String rootDirectoryPath, List<FileType> fileTypes) throws FileNotFoundException {
        this.rootDirectory = new File(rootDirectoryPath);
        if (!rootDirectory.exists() || !rootDirectory.isDirectory()) {
            throw new FileNotFoundException("Directory does not exists!");
        }

        this.fileTypes = fileTypes;
    }

    public void zipAll() throws IOException {
        Map<FileType, List<File>> filesByType = new FilesByTypeFinder(rootDirectory, fileTypes).getFoundFiles();
        for (FileType key : filesByType.keySet()) {
            List<File> files = filesByType.get(key);

            if (!files.isEmpty()) {
                File zipDirectory = createZipDirectory(key);
                FilesZipper zipper = new FilesZipper(files, zipDirectory);
                zipper.zip();
            }
        }
    }

    private File createZipDirectory(FileType type) {
        int index = rootDirectory.getPath().lastIndexOf("\\");
        String upperDirectoryPath = rootDirectory.getPath().substring(0, index + 1);
        String zipDirectoryPath = upperDirectoryPath + type.name().toLowerCase() + ".zip";

        return new File(zipDirectoryPath);
    }
}