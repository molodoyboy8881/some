package com.geekhub.hw8.variant2;

import com.geekhub.hw8.variant2.zip.FileType;
import com.geekhub.hw8.variant2.zip.FileZipMarket;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean pathCorrect = false;
        List<FileType> fileTypes = List.of(FileType.IMAGE, FileType.AUDIO, FileType.VIDEO);

        while (!pathCorrect) {
            pathCorrect = true;
            System.out.print("Enter path of directory: ");
            String rootDirectoryPath = scanner.nextLine();
            try {
                FileZipMarket market = new FileZipMarket(rootDirectoryPath, fileTypes);
                market.zipAll();
            } catch (IOException e) {
                pathCorrect = false;
                System.out.println("Enter correct path!");
            }
        }
    }
}