package com.geekhub.hw8.variant1.zip;

import java.io.*;

public class FileZipMarket {
    private final File rootDirectory;

    public FileZipMarket(String rootDirectoryPath) throws FileNotFoundException {
        rootDirectory = new File(rootDirectoryPath);
        if (!rootDirectory.exists() || !rootDirectory.isDirectory()) {
            throw new FileNotFoundException();
        }
    }

    public void zipAll() throws IOException {
        FileType[] types = {FileType.AUDIO, FileType.VIDEO, FileType.IMAGE};
        for (FileType type : types) {
            FileFilter filter = initFileFilter(type);
            File zippedDirectory = initZippedDirectory(type);
            DirectoryZipper zipper = new DirectoryZipper(rootDirectory, zippedDirectory, filter);
            zipper.zip();
        }
    }

    private FileFilter initFileFilter(FileType type) {
        return file -> {
            if (file.isDirectory()) {
                return true;
            }

            int index = file.getName().lastIndexOf(".");
            if (index == -1 || index == 0) {
                return false;
            }

            String extension = file.getName().substring(index + 1);
            return type.getExtensions().contains(extension);
        };
    }

    private File initZippedDirectory(FileType type) {
        int index = rootDirectory.getPath().lastIndexOf("\\");
        String upperDirectoryPath = rootDirectory.getPath().substring(0, index + 1);
        String zipDirectoryPath = upperDirectoryPath + type.name().toLowerCase() + ".zip";

        return new File(zipDirectoryPath);
    }
}
