package com.geekhub.hw8.variant1.zip;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class DirectoryZipper {
    private final File rootDirectory;
    private final File zippedDirectory;
    private final FileFilter fileFilter;

    public DirectoryZipper(File rootDirectory, File zippedDirectory, FileFilter fileFilter) {
        this.rootDirectory = rootDirectory;
        this.zippedDirectory = zippedDirectory;
        this.fileFilter = fileFilter;
    }

    public void zip() throws IOException {
        try {
            findAnyFile(rootDirectory);
        } catch (RecursionBreakException e) {
            try(ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zippedDirectory))) {
                zipDirectory(rootDirectory, rootDirectory.getName(), zos);
            }
        }
    }

    private void findAnyFile(File file) throws RecursionBreakException {
        if (file.isDirectory()) {
            File[] subFiles = file.listFiles(fileFilter);
            if(subFiles != null) {
                for (File subFile : subFiles) {
                    findAnyFile(subFile);
                }
            }
        } else {
            throw new RecursionBreakException("File found!");
        }
    }

    private void zipDirectory(File file, String fileName, ZipOutputStream zos) throws IOException {
        if (file.isDirectory()) {
            zos.putNextEntry(new ZipEntry(fileName + "/"));
            zos.closeEntry();

            File[] subFiles = file.listFiles(fileFilter);
            if (subFiles != null) {
                for (File subFile : subFiles) {
                    zipDirectory(subFile, fileName + "/" + subFile.getName(), zos);
                }
            }

            return;
        }

        try(FileInputStream fis = new FileInputStream(file)) {
            ZipEntry entry = new ZipEntry(fileName);
            zos.putNextEntry(entry);

            byte[] bytes = new byte[1024];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                zos.write(bytes, 0, length);
            }

            zos.closeEntry();
        }
    }
}