package com.geekhub.hw8.variant1.zip;

public class RecursionBreakException extends RuntimeException{

    public RecursionBreakException(String message) {
        super(message);
    }
}
