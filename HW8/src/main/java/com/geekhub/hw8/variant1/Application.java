package com.geekhub.hw8.variant1;

import com.geekhub.hw8.variant1.zip.FileZipMarket;

import java.io.IOException;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean pathCorrect = false;

        while (!pathCorrect) {
            pathCorrect = true;
            System.out.print("Enter path of directory: ");
            String rootDirectoryPath = scanner.nextLine();

            try {
                FileZipMarket market = new FileZipMarket(rootDirectoryPath);
                market.zipAll();
            } catch (IOException e) {
                pathCorrect = false;
                System.out.println("Enter correct path of a directory!");
            }
        }
    }
}
