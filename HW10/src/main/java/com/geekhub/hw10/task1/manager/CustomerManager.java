package com.geekhub.hw10.task1.manager;

import com.geekhub.hw10.task1.data.Customer;
import org.postgresql.util.PSQLException;

import java.sql.*;

public class CustomerManager {
    private final PreparedStatement insert;
    private final PreparedStatement delete;
    private final PreparedStatement update;

    public CustomerManager(Connection connection) throws SQLException {
        insert = connection.prepareStatement("insert into customers " +
                "values (?,?,?,?)");
        delete = connection.prepareStatement("delete from customers where id = ?");
        update = connection.prepareStatement("update customers set first_name = ?"
                + ", last_name = ?, cell_phone = ? where id = ?");
    }

    public boolean register(Customer customer) throws SQLException {
        try {
            insert.setInt(1, customer.getId());
            insert.setString(2, customer.getFirstName());
            insert.setString(3, customer.getLastName());
            insert.setString(4, customer.getCellPhone());
            insert.executeUpdate();
        } catch (PSQLException e) {
            return false;
        }

        return true;
    }

    public boolean delete(int id) throws SQLException {
        delete.setInt(1, id);
        int rows = delete.executeUpdate();
        return rows > 0;
    }

    public boolean updateCustomer(int customerId, Customer newCustomer) throws SQLException {
        String firstName = newCustomer.getFirstName();
        String lastName = newCustomer.getLastName();
        String cellPhone = newCustomer.getCellPhone();

        update.setString(1, firstName);
        update.setString(2, lastName);
        update.setString(3, cellPhone);
        update.setInt(4, customerId);

        return update.executeUpdate() > 0;
    }
}
