package com.geekhub.hw10.task1.data;

import java.util.List;

public class Order {
    private final int id;
    private final Customer customer;
    private final List<Product> products;
    private final List<Integer> quantities;
    private final List<Integer> prices;
    private final String delivery;

    public Order(int id, Customer customer, List<Product> products,
                 List<Integer> quantities, List<Integer> prices, String delivery) {
        this.id = id;
        this.customer = customer;
        this.products = products;
        this.quantities = quantities;
        this.prices = prices;
        this.delivery = delivery;
    }

    public int getId() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public List<Product> getProducts() {
        return products;
    }

    public List<Integer> getQuantities() {
        return quantities;
    }

    public List<Integer> getPrices() {
        return prices;
    }

    public String getDelivery() {
        return delivery;
    }
}
