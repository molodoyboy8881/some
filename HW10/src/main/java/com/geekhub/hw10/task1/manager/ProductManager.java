package com.geekhub.hw10.task1.manager;

import com.geekhub.hw10.task1.data.Product;
import org.postgresql.util.PSQLException;

import java.sql.*;

public class ProductManager {
    private final PreparedStatement insert;
    private final PreparedStatement delete;
    private final PreparedStatement update;

    public ProductManager(Connection connection) throws SQLException {
        insert = connection.prepareStatement("insert into products " +
                "values (?,?,?,?)");
        delete = connection.prepareStatement("delete from products where id = ?");
        update = connection.prepareStatement("update products set name = ?"
                + ", description = ? , current_price = ?  where id = ?");
    }

    public boolean addProduct(Product product) throws SQLException {
        try {
            insert.setInt(1, product.getId());
            insert.setString(2, product.getName());
            insert.setString(3, product.getDescription());
            insert.setInt(4, product.getCurrentPrice());
            insert.executeUpdate();
        } catch (PSQLException e) {
            return false;
        }

        return true;
    }

    public boolean deleteProduct(int id) throws SQLException {
        delete.setInt(1, id);
        int rows = delete.executeUpdate();
        return rows > 0;
    }

    public boolean updateProduct(int productId, Product newProduct) throws SQLException {
        String name = newProduct.getName();
        String description = newProduct.getDescription();
        int price = newProduct.getCurrentPrice();

        update.setString(1, name);
        update.setString(2, description);
        update.setInt(3, price);
        update.setInt(4, productId);

        return update.executeUpdate() > 0;
    }
}
