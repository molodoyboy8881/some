package com.geekhub.hw10.task1;

import com.geekhub.hw10.task1.data.Order;
import com.geekhub.hw10.task1.data.Product;
import com.geekhub.hw10.task1.data.Customer;
import com.geekhub.hw10.task1.manager.CustomerManager;
import com.geekhub.hw10.task1.manager.OrderManager;
import com.geekhub.hw10.task1.manager.ProductManager;

import java.sql.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Application {
    public static void main(String[] args) throws SQLException {
        try(Connection connection = getConnection()) {
            CustomerManager customerManager = new CustomerManager(connection);
            Customer customer1 = new Customer(1, "Bob", "Johnson", "12345");
            Customer customer2 = new Customer(2, "John", "Dowson", "2255");
            customerManager.register(customer1);
            customerManager.register(customer2);

            ProductManager productManager = new ProductManager(connection);
            Product product1 = new Product(1, "Apple", "healthy", 20);
            Product product2 = new Product(2, "Milk", "sweet", 30);
            Product product3 = new Product(3, "Cake", "tasty", 40);
            Product product4 = new Product(4, "Rice", "", 45);
            productManager.addProduct(product1);
            productManager.addProduct(product2);
            productManager.addProduct(product3);
            productManager.addProduct(product4);

            OrderManager orderManager = new OrderManager(connection);
            Order order1 = new Order(1, customer1, List.of(product1, product2),
                    List.of(10, 20), List.of(20, 30), "Kyiv");
            Order order2 = new Order(2, customer2, List.of(product3, product4),
                    List.of(11, 13), List.of(40, 45), "Lviv");
            Order order3 = new Order(3, customer1, List.of(product3), List.of(11), List.of(40), "Odessa");
            orderManager.makeOrder(order1);
            orderManager.makeOrder(order2);
            orderManager.makeOrder(order3);

            System.out.println(getSpentMoneyByCustomers(connection));
            System.out.println(getTheMostPopularProduct(connection));
        }
    }

    private static Connection getConnection() throws SQLException {
        String url = "jdbc:postgresql://localhost:5432/postgres";
        String userName = "postgres";
        String password = "12345";
        try {
            return DriverManager.getConnection(url, userName, password);
        } catch (SQLException e) {
            throw new SQLException();
        }
    }

    private static Product getTheMostPopularProduct(Connection connection) throws SQLException {
        String query = "select " +
                            "count(poo.product_id) as product_count, " +
                            "p.id, p.name, p.description, p.current_price " +
                        "from " +
                            "products p " +
                            "join products_of_orders poo on p.id = poo.product_id " +
                        "group by " +
                            "p.id, p.name, p.description, p.current_price " +
                        "order by product_count desc limit 1";

        Statement statement = connection.createStatement();
        try(ResultSet resultSet = statement.executeQuery(query)) {
            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");
                int price = resultSet.getInt("current_price");
                return new Product(id, name, description, price);
            }
        }

        return null;
    }

    private static Map<Customer, Integer> getSpentMoneyByCustomers(Connection connection) throws SQLException {
        Map<Customer, Integer> spentMoneyByCustomers = new HashMap<>();
        String query = "select " +
                            "sum(poo.product_price * poo.product_quantity) as spent_money, " +
                            "customer_id, " +
                            "c.first_name, c.last_name, c.cell_phone " +
                        "from " +
                                "products_of_orders poo " +
                                "join orders o on poo.order_id = o.id " +
                                "join customers c on o.customer_id = c.id " +
                        "group by " +
                            "customer_id, c.first_name, c.last_name, c.cell_phone";

        Statement statement = connection.createStatement();
        try(ResultSet resultSet = statement.executeQuery(query)) {
            while (resultSet.next()) {
                int id = resultSet.getInt("customer_id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String cellPhone = resultSet.getString("cell_phone");
                int spentMoney = resultSet.getInt("spent_money");

                Customer customer = new Customer(id, firstName, lastName, cellPhone);
                spentMoneyByCustomers.put(customer, spentMoney);
            }
        }
        return spentMoneyByCustomers;
    }
}
