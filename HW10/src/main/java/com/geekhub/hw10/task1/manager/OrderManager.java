package com.geekhub.hw10.task1.manager;

import com.geekhub.hw10.task1.data.Order;
import com.geekhub.hw10.task1.data.Product;
import org.postgresql.util.PSQLException;

import java.sql.*;
import java.util.Iterator;
import java.util.List;

public class OrderManager {
    private final Statement statement;
    private final PreparedStatement insert;
    private final PreparedStatement delete;
    private final PreparedStatement update;
    private final PreparedStatement insertProductsOfOrder;
    private final PreparedStatement deleteProductsOfOrder;

    public OrderManager(Connection connection) throws SQLException {
        statement = connection.createStatement();
        insert = connection.prepareStatement("insert into orders " +
                "values (?,?,?)");
        delete = connection.prepareStatement("delete from orders where id = ?");
        update = connection.prepareStatement("update orders set "
                + " delivery_place = ? where id = ?");
        insertProductsOfOrder = connection.prepareStatement("insert into products_of_orders " +
                "values (?,?,?,?)");
        deleteProductsOfOrder = connection.prepareStatement("delete from products_of_orders where order_id = ?");
    }

    public boolean makeOrder(Order order) throws SQLException {
        if (!existenceOFCustomer(order.getCustomer().getId())) {
            return false;
        } else if (!existenceOfProducts(order.getProducts())) {
            return false;
        } else if (!insertOrder(order)) {
            return false;
        }

        Iterator<Product> productIt = order.getProducts().iterator();
        Iterator<Integer> quantityIt = order.getQuantities().iterator();
        Iterator<Integer> priceIt = order.getPrices().iterator();
        while (productIt.hasNext()) {
            insertProductsOfOrder(order.getId(), productIt.next().getId(), quantityIt.next(), priceIt.next());
        }

        return true;
    }

    public boolean cancelOrder(int id) throws SQLException {
        deleteProductsOfOrder.setInt(1, id);
        deleteProductsOfOrder.executeUpdate();
        delete.setInt(1, id);
        int rows = delete.executeUpdate();

        return rows > 0;
    }

    public void updateOrder(int orderId, Order newOrder) throws SQLException {
        String delivery = newOrder.getDelivery();

        Iterator<Product> productIt = newOrder.getProducts().iterator();
        Iterator<Integer> quantityIt = newOrder.getQuantities().iterator();
        Iterator<Integer> priceIt = newOrder.getPrices().iterator();
        while (productIt.hasNext()) {
            insertProductsOfOrder(orderId, productIt.next().getId(), quantityIt.next(), priceIt.next());
        }

        update.setString(1, delivery);
        update.setInt(2, orderId);
        update.executeUpdate();
    }


    private boolean existenceOFCustomer(int id) throws SQLException {
        String query = "select count(1) from customers where id = " + id;

        return statement.executeQuery(query).next();
    }

    private boolean existenceOfProducts(List<Product> products) throws SQLException {
        for (Product product : products) {
            String query = "select count(1) from products where id = " + product.getId();

            if (!statement.executeQuery(query).next()) {
                return false;
            }
        }

        return true;
    }

    private boolean insertOrder(Order order) throws SQLException {
        try {
            insert.setInt(1, order.getId());
            insert.setInt(2, order.getCustomer().getId());
            insert.setString(3, order.getDelivery());
            insert.executeUpdate();
        } catch (PSQLException e) {
            return false;
        }

        return true;
    }

    private void insertProductsOfOrder(int orderId, int productId, int quantity, int price) throws SQLException {
        insertProductsOfOrder.setInt(1, orderId);
        insertProductsOfOrder.setInt(2, productId);
        insertProductsOfOrder.setInt(3, quantity);
        insertProductsOfOrder.setInt(4, price);
        insertProductsOfOrder.executeUpdate();
    }
}
