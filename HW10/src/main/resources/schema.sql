CREATE DATABASE postgres;

CREATE TABLE customers
(
    id         integer UNIQUE primary key NOT NULL,
    first_name character varying(100)     NOT NULL,
    last_name  character varying(100)     NOT NULL,
    cell_phone character varying(20)
);

CREATE TABLE products
(
    id            integer UNIQUE primary key NOT NULL,
    name          character varying(100)     NOT NULL,
    description   character varying(300),
    current_price integer                    NOT NULL
);

CREATE TABLE orders
(
    id             integer UNIQUE primary key NOT NULL,
    customer_id    integer references customers (id),
    delivery_place character varying(200)     NOT NULL
);

CREATE TABLE products_of_orders
(
    order_id         integer references orders (id),
    product_id       integer references products (id),
    product_quantity integer NOT NULL,
    product_price    integer NOT NULL
);