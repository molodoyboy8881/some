package com.geekhub.hw1.application;

import com.geekhub.hw1.number_sum_calculator.NumberSumCalculator;
import com.geekhub.hw1.phone_validation.PhoneNumberValidator;

import java.util.ArrayList;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean correctNumber = false;

        do {
            System.out.println("Please enter the phone number: ");

            String phoneNumber = scanner.nextLine();
            PhoneNumberValidator validator = new PhoneNumberValidator(phoneNumber);
            if (validator.isPhoneNumberValid() != null) {
                correctNumber = true;
            }

            if (correctNumber) {
                System.out.println("Phone number is correct.");

                NumberSumCalculator calculator = new NumberSumCalculator(Long.parseLong(phoneNumber));
                ArrayList<String> answer = calculator.calculate();
                for (String string : answer) {
                    System.out.print(string);
                }
            } else {
                System.out.print("Phone number is incorrect. ");
            }
        } while (!correctNumber);
    }
}
