package com.geekhub.hw1.number_sum_calculator;

import java.util.ArrayList;

public class NumberSumCalculator {
    private long number;

    public NumberSumCalculator(long number) {
        this.number = number;
    }

    public ArrayList<String> calculate() {
        int sum;
        int counter = 0;
        ArrayList<String> answer = new ArrayList<>();
        for (sum = 0; number > 9; number = sum) {
            for (sum = 0; number > 0; number /= 10) {
                sum += number % 10;
            }
            answer.add(++counter + "st round of calculation, sum is: " + sum + "\n");
        }

        answer.add("Final result is: ");
        finalAnswer(answer, sum);

        return answer;
    }

    private void finalAnswer(ArrayList<String> answer, int sum) {
        switch (sum) {
            case 1:
                answer.add("One\n");
                break;
            case 2:
                answer.add("Two\n");
                break;
            case 3:
                answer.add("Three\n");
                break;
            case 4:
                answer.add("Four\n");
                break;
            default:
                answer.add(sum + "\n");
                break;
        }
    }
}
