package com.geekhub.hw1.phone_validation;

public class PhoneNumberValidator {
    private final String phoneNumber;
    private final static String countryCode1 = "8";
    private final static String countryCode2 = "380";
    private final static String countryCode3 = "+380";
    private final static String countryCode4 = "00380";

    public PhoneNumberValidator(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Long isPhoneNumberValid() {
        if (isNumberFormatValid() && isCountryCodeAndOperatorValid()) {
            return Long.parseLong(phoneNumber);
        }

        return null;
    }

    private boolean isCountryCodeAndOperatorValid() {
        int length = phoneNumber.length();

        if (length == 14 && phoneNumber.startsWith(countryCode4)) {
            return isOperatorValid(phoneNumber.substring(4, 7));
        } else if (length == 13 && phoneNumber.startsWith(countryCode3)) {
            return isOperatorValid(phoneNumber.substring(3, 6));
        } else if (length == 12 && phoneNumber.startsWith(countryCode2)) {
            return isOperatorValid(phoneNumber.substring(2, 5));
        } else if (length == 11 && phoneNumber.startsWith(countryCode1)) {
            return isOperatorValid(phoneNumber.substring(1, 4));
        } else if (length == 10) {
            return isOperatorValid(phoneNumber.substring(0, 3));
        }

        return false;
    }

    private boolean isOperatorValid(String operatorPrefix) {
        int operator = Integer.parseInt(operatorPrefix);
        if ((operator >= 91) && (operator <= 99)) {
            return true;
        } else if ((operator >= 66) && (operator <= 68)) {
            return true;
        }

        return ((operator == 63) || (operator == 39) || (operator == 50));
    }

    private boolean isNumberFormatValid() {
        try {
            Long.parseLong(phoneNumber);
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }
}
