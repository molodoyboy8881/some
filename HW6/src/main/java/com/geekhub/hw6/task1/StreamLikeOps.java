package com.geekhub.hw6.task1;

import java.util.*;
import java.util.function.*;

public class StreamLikeOps {

    private StreamLikeOps() {
    }

    public static <E> List<E> generate(Supplier<E> generator, Supplier<List<E>> listFactory, int count) {
        List<E> list = listFactory.get();
        for (int i = 0; i < count; ++i) {
            list.add(generator.get());
        }

        return list;
    }

    public static <E> List<E> filter(List<E> elements, Predicate<E> filter) {
        if (elements.size() == 0) {
            return Collections.emptyList();
        }

        List<E> result = new ArrayList<>();
        for (E element : elements) {
            if (filter.test(element)) {
                result.add(element);
            }
        }

        return result;
    }

    public static <E> boolean anyMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if (predicate.test(element)) {
                return true;
            }
        }

        return false;
    }


    public static <E> boolean allMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if (!predicate.test(element)) {
                return false;
            }
        }

        return true;
    }

    public static <E> boolean noneMatch(List<E> elements, Predicate<E> predicate) {
        return allMatch(elements, predicate.negate());
    }

    public static <T, R> List<R> map(List<T> elements, Function<T, R> mappingFunction, Supplier<List<R>> listFactory) {
        List<R> result = listFactory.get();
        for (T element : elements) {
            result.add(mappingFunction.apply(element));
        }

        return result;
    }

    public static <E> Optional<E> max(List<E> elements, Comparator<E> comparator) {
        if (elements.size() == 0) {
            return Optional.empty();
        }

        Iterator<E> iterator = elements.iterator();
        E max = iterator.next();

        while (iterator.hasNext()) {
            E element = iterator.next();
            if (comparator.compare(max, element) < 0) {
                max = element;
            }
        }

        return Optional.of(max);
    }

    public static <E> Optional<E> min(List<E> elements, Comparator<E> comparator) {
        return max(elements, comparator.reversed());
    }

    public static <E> List<E> distinct(List<E> elements, Supplier<List<E>> listFactory) {
        List<E> result = listFactory.get();
        Set<E> distinctElements = new HashSet<>();
        for (E element : elements) {
            if (distinctElements.add(element)) {
                result.add(element);
            }
        }

        return result;
    }

    public static <E> void forEach(List<E> elements, Consumer<E> consumer) {
        for (E element : elements) {
            consumer.accept(element);
        }
    }

    public static <E> Optional<E> reduce(List<E> elements, BinaryOperator<E> accumulator) {
        Iterator<E> iterator = elements.iterator();
        if (iterator.hasNext()) {
            E result = reduce(iterator.next(), elements.subList(1, elements.size()), accumulator);
            return Optional.of(result);
        }

        return Optional.empty();
    }

    public static <E> E reduce(E seed, List<E> elements, BinaryOperator<E> accumulator) {
        for (E element : elements) {
            seed = accumulator.apply(seed, element);
        }

        return seed;
    }

    public static <E> Map<Boolean, List<E>> partitionBy(List<E> elements, Predicate<E> predicate,
                                                        Supplier<Map<Boolean, List<E>>> mapFactory,
                                                        Supplier<List<E>> listFactory) {
        return partitionByAndMapElement(elements, predicate, mapFactory, listFactory, Function.identity());
    }

    public static <T, K> Map<K, List<T>> groupBy(List<T> elements, Function<T, K> classifier,
                                                 Supplier<Map<K, List<T>>> mapFactory, Supplier<List<T>> listFactory) {
        return groupByAndMapElement(elements, classifier, mapFactory, listFactory, Function.identity());
    }

    public static <T, K, U> Map<K, U> toMap(List<T> elements, Function<T, K> keyFunction, Function<T, U> valueFunction,
                                            BinaryOperator<U> mergeFunction, Supplier<Map<K, U>> mapFactory) {
        Map<K, U> map = mapFactory.get();
        for (T element : elements) {
            K key = keyFunction.apply(element);
            U newValue = valueFunction.apply(element);
            U oldValue = map.get(key);
            if (oldValue != null) {
                U mergeValue = mergeFunction.apply(oldValue, newValue);
                map.replace(key, mergeValue);
            } else {
                map.put(key, newValue);
            }
        }

        return map;
    }

    public static <E, T> Map<Boolean, List<T>> partitionByAndMapElement(List<E> elements, Predicate<E> predicate,
                                                                        Supplier<Map<Boolean, List<T>>> mapFactory,
                                                                        Supplier<List<T>> listFactory,
                                                                        Function<E, T> elementMapper) {
        Map<Boolean, List<T>> map = mapFactory.get();
        map.put(false,listFactory.get());
        map.put(true,listFactory.get());
        for (E element : elements) {
            boolean key = predicate.test(element);
            List<T> value = map.get(key);

            T mappedElement = elementMapper.apply(element);
            value.add(mappedElement);
        }

        return map;
    }

    public static <T, U, K> Map<K, List<U>> groupByAndMapElement(List<T> elements, Function<T, K> classifier,
                                                                 Supplier<Map<K, List<U>>> mapFactory,
                                                                 Supplier<List<U>> listFactory,
                                                                 Function<T, U> elementMapper) {
        Map<K, List<U>> map = mapFactory.get();
        for (T element : elements) {
            K key = classifier.apply(element);
            List<U> value = map.get(key);
            if (value == null) {
                value = listFactory.get();
                map.put(key, value);
            }

            U mappedElement = elementMapper.apply(element);
            value.add(mappedElement);
        }

        return map;
    }
}
