package com.geekhub.hw6.task1;

import java.util.*;
import java.util.function.BinaryOperator;

public class Application {
    public static void main(String[] args) {
        List<Person> people = List.of(new Person(5, "Andy")
                , new Person(12, "Bob")
                , new Person(18, "Cindy")
                , new Person(18, "Cindy")
                , new Person(22, "Emma")
                , new Person(22, "Emma")
                , new Person(32, "Franck"));

        Random random = new Random();
        List<Integer> list1 = StreamLikeOps.generate(random::nextInt, ArrayList::new, 5);
        System.out.println("generate result: " + list1);

        List<Person> list2 = StreamLikeOps.filter(people, person -> person.getAge() > 18);
        System.out.println("filter result: " + list2);

        boolean anyMatched = StreamLikeOps.anyMatch(people, person -> person.getAge() == 100);
        System.out.println("anyMatched result: " + anyMatched);

        boolean allMatched = StreamLikeOps.allMatch(people, person -> person.getAge() > 1);
        System.out.println("allMatched result: " + allMatched);

        boolean noneMatched = StreamLikeOps.noneMatch(people, person -> person.getName().equals("Emma"));
        System.out.println("noneMatched result: " + noneMatched);

        List<String> list3 = StreamLikeOps.map(people, person -> person.getName() + "$", ArrayList::new);
        System.out.println("map result: " + list3);

        Comparator<Person> personComparator = Comparator.naturalOrder();
        Optional<Person> maxPerson = StreamLikeOps.max(people, personComparator);
        System.out.print("max result: ");
        maxPerson.ifPresent(System.out::println);

        Optional<Person> minPerson = StreamLikeOps.min(people, personComparator);
        System.out.print("min result: ");
        minPerson.ifPresent(System.out::println);

        List<Person> list4 = StreamLikeOps.distinct(people, ArrayList::new);
        System.out.println("distinct result: " + list4);

        System.out.print("forEach result: ");
        StreamLikeOps.forEach(people, person -> System.out.print(person.getAge() * 2 + " "));
        System.out.println();

        BinaryOperator<Person> accumulator = (person1, person2) -> {
            if (person1.compareTo(person2) > 0) {
                return person1;
            }

            return person2;
        };
        System.out.print("reduce result: ");
        Optional<Person> reducePeople = StreamLikeOps.reduce(people, accumulator);
        reducePeople.ifPresent(System.out::println);

        Person p = StreamLikeOps.reduce(new Person(1, "Oleg"), people, (person1, person2) -> person1);
        System.out.println("reduce seed result: " + p);

        Map<Boolean, List<Person>> map1 = StreamLikeOps.partitionBy(people, person -> person.getAge() > 18, LinkedHashMap::new
                , ArrayList::new);
        System.out.println("partitionBy result: " + map1);

        Map<String, List<Person>> map2 = StreamLikeOps.groupBy(people, Person::getName, LinkedHashMap::new, ArrayList::new);
        System.out.println("groupBy result: " + map2);

        BinaryOperator<String> mergeFunction = (person1, person2) -> person1 + " " + person2;
        Map<Integer, String> map3 = StreamLikeOps.toMap(people, Person::getAge, Person::getName, mergeFunction, LinkedHashMap::new);
        System.out.println("toMap result: " + map3);

        Map<Boolean, List<String>> map4 = StreamLikeOps.partitionByAndMapElement(people, person -> person.getAge() > 18
                , LinkedHashMap::new, ArrayList::new, Person::getName);
        System.out.println("partitionByAndMapElement result: " + map4);

        Map<Integer, List<String>> map5 = StreamLikeOps.groupByAndMapElement(people, Person::getAge
                , LinkedHashMap::new, LinkedList::new, Person::getName);
        System.out.println("groupByAndMapElement result: " + map5);
    }
}
