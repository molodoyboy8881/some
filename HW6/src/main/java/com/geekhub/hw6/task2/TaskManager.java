package com.geekhub.hw6.task2;

import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TaskManager {
    public List<Task> find5NearestImportantTasks(List<Task> tasks) {
        return tasks.stream()
                .filter(task -> task.getType() == TaskType.IMPORTANT && !task.isDone())
                .filter(task -> task.getStartDate().isAfter(LocalDate.now()) || task.getStartDate().isEqual(LocalDate.now()))
                .sorted(Comparator.comparing(Task::getStartDate))
                .limit(5)
                .collect(Collectors.toList());
    }

    public List<String> getUniqueCategories(List<Task> tasks) {
        return tasks.stream()
                .flatMap(task -> task.getCategories().stream())
                .distinct()
                .collect(Collectors.toList());
    }

    public Map<String, List<Task>> getCategoriesWithTasks(List<Task> tasks) {
        return tasks.stream()
                .flatMap(task -> task.getCategories().stream()
                        .map(categories -> Map.entry(categories, task)))
                .collect(Collectors.groupingBy(Map.Entry::getKey
                        , Collectors.mapping(Map.Entry::getValue, Collectors.toList())));
    }

    public Map<Boolean, List<Task>> splitTasksIntoDoneAndInProgress(List<Task> tasks) {
        return tasks.stream()
                .collect(Collectors.groupingBy(Task::isDone));
    }

    public boolean existsTaskOfCategory(List<Task> tasks, String category) {
        return tasks.stream()
                .anyMatch(task -> task.getCategories().contains(category));
    }

    public String getTitlesOfTasks(List<Task> tasks, int startNo, int endNo) {
        return tasks.stream()
                .limit(startNo - endNo)
                .map(Task::getTitle)
                .collect(Collectors.joining(","));
    }

    public Task findTaskWithBiggestCountOfCategories(List<Task> tasks) {
       Task max = tasks.stream()
                .max(Comparator.comparing(task -> task.getCategories().size()))
                .orElseThrow();

       return tasks.stream()
               .filter(task -> task.getCategories().size() == max.getCategories().size())
               .max(Comparator.comparing(Task::getStartDate))
               .orElseThrow();
    }

    public IntSummaryStatistics getCategoriesNamesLengthStatistics(List<Task> tasks) {
        return tasks.stream()
                .flatMap(task -> task.getCategories().stream())
                .distinct()
                .collect(Collectors.summarizingInt(String::length));
    }

    public Map<String, Long> getCountsByCategories(List<Task> tasks) {
        return tasks.stream()
                .flatMap(task -> task.getCategories().stream())
                .collect(Collectors.toMap(Function.identity(), value -> 1L, Long::sum));
    }
}
