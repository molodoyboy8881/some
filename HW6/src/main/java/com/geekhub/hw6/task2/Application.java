package com.geekhub.hw6.task2;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public class Application {
    public static void main(String[] args) {
        Set<String> categories1 = Set.of("work", "walk");
        Set<String> categories2 = Set.of("clean up", "football");
        Set<String> categories3 = Set.of("work", "clean up");
        Set<String> categories4 = Set.of("football", "walk", "nothing");

        Task task1 = new Task(101, TaskType.IMPORTANT, "T1", true
                , categories1, LocalDate.of(2020, 11, 1));
        Task task2 = new Task(102, TaskType.IMPORTANT, "T2", true
                , categories1, LocalDate.of(2020, 11, 25));
        Task task3 = new Task(103, TaskType.IMPORTANT, "T3", false
                , categories2, LocalDate.of(2020, 12, 10));
        Task task4 = new Task(104, TaskType.IMPORTANT, "T4", false
                , categories2, LocalDate.of(2020, 12, 11));
        Task task5 = new Task(105, TaskType.IMPORTANT, "T5", false
                , categories3, LocalDate.of(2020, 12, 14));
        Task task6 = new Task(106, TaskType.IMPORTANT, "T6", false
            , categories3, LocalDate.of(2020, 12, 20));
        Task task7 = new Task(107, TaskType.IMPORTANT, "T7", false
                , categories4, LocalDate.of(2020, 12, 15));
        Task task8 = new Task(108, TaskType.IMPORTANT, "T8", false
                , categories4, LocalDate.of(2020, 12, 17));

        TaskManager manager = new TaskManager();
        List<Task> tasks = List.of(task1, task2, task3, task4, task5, task6, task7, task8);

        System.out.println("5 nearest " + manager.find5NearestImportantTasks(tasks));
        System.out.println("unique categories " + manager.getUniqueCategories(tasks));
        System.out.println("categories with tasks " + manager.getCategoriesWithTasks(tasks));
        System.out.println("done/inProgress split " + manager.splitTasksIntoDoneAndInProgress(tasks));
        System.out.println("exists of category " + manager.existsTaskOfCategory(tasks, "cook"));
        System.out.println("titles of tasks " + manager.getTitlesOfTasks(tasks, 0, 3));
        System.out.println("max counts of categories " + manager.findTaskWithBiggestCountOfCategories(tasks));
        System.out.println("statistics " + manager.getCategoriesNamesLengthStatistics(tasks));
        System.out.println("counts of categories " + manager.getCountsByCategories(tasks));
    }
}
