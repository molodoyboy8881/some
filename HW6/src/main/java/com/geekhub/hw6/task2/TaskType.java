package com.geekhub.hw6.task2;

public enum TaskType {
    INCONSIDERABLE,
    AVERAGE,
    IMPORTANT;
}
