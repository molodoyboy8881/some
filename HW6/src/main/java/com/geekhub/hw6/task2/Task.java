package com.geekhub.hw6.task2;

import java.time.LocalDate;
import java.util.Set;

public class Task {
    private final int id;
    private final TaskType type;
    private final String title;
    private final boolean done;
    private final Set<String> categories;
    private final LocalDate startDate;

    public Task(int id, TaskType type, String title, boolean done, Set<String> categories, LocalDate startDate) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.done = done;
        this.categories = categories;
        this.startDate = startDate;
    }

    public int getId() {
        return id;
    }

    public TaskType getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public boolean isDone() {
        return done;
    }

    public Set<String> getCategories() {
        return categories;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}
