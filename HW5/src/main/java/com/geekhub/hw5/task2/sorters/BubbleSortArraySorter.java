package com.geekhub.hw5.task2.sorters;

import com.geekhub.hw5.task2.direction.Direction;

import java.util.Comparator;

public class BubbleSortArraySorter implements ArraySorter {
    @Override
    public <T extends Comparable<T>> void sort(T[] array, Direction direction) {
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1; ++i) {
                if (chooseDirection(array[i].compareTo(array[i + 1]), direction)) {
                    isSorted = false;

                    T swap = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = swap;
                }
            }
        }
    }

    @Override
    public <T> void sort(T[] array, Comparator<T> comparator, Direction direction) {
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length - 1; ++i) {
                if (chooseDirection(comparator.compare(array[i], array[i + 1]), direction)) {
                    isSorted = false;

                    T swap = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = swap;
                }
            }
        }
    }
}
