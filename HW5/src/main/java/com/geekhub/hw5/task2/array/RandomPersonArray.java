package com.geekhub.hw5.task2.array;

import com.geekhub.hw5.task2.person.Person;

import java.util.Random;

public class RandomPersonArray {
    private final int length;
    private final static String[] namePool = {"Ablert", "Bob", "Cilena", "David", "Emma", "Jack", "Kris",
            "Lina", "Robert", "Steve"};

    public RandomPersonArray(int length) {
        this.length = length;
    }

    public Person[] createRandomArray() {
        Random random = new Random();
        Person[] array = new Person[length];
        for (int i = 0; i < length; ++i) {
            String name = namePool[random.nextInt(10)];
            int age = random.nextInt(100);
            array[i] = new Person(name, age);
        }

        return array;
    }
}
