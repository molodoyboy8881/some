package com.geekhub.hw5.task2.person;

import java.util.Comparator;

public class PersonNameThenAgeComparator implements Comparator<Person> {
    @Override
    public int compare(Person o1, Person o2) {
        int cmp = o1.getName().compareTo(o2.getName());
        if (cmp != 0) {
            return cmp;
        }

        return Integer.compare(o1.getAge(), o2.getAge());
    }
}
