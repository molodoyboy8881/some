package com.geekhub.hw5.task2.sorters;

import com.geekhub.hw5.task2.direction.Direction;

import java.util.Comparator;

public interface ArraySorter {
    <T extends Comparable<T>> void sort(T[] array, Direction direction);

    <T> void sort(T[] array, Comparator<T> comparator, Direction direction);

    default boolean chooseDirection(int compareResult, Direction direction) {
        return compareResult * direction.getDirection() > 0;
    }
}