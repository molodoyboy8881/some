package com.geekhub.hw5.task2.application;

import com.geekhub.hw5.task2.array.RandomPersonArray;
import com.geekhub.hw5.task2.direction.Direction;
import com.geekhub.hw5.task2.person.Person;
import com.geekhub.hw5.task2.person.PersonAgeThenNameComparator;
import com.geekhub.hw5.task2.person.PersonNameThenAgeComparator;
import com.geekhub.hw5.task2.sorters.BubbleSortArraySorter;
import com.geekhub.hw5.task2.sorters.InsertionSortArraySorter;

import java.util.Arrays;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        System.out.print("Enter direction(ASC or DSC): ");
        Scanner scanner = new Scanner(System.in);
        String directionValue = scanner.nextLine().toUpperCase();
        Direction direction = Direction.valueOf(directionValue);

        Person[] array1 = new RandomPersonArray(5).createRandomArray();
        new BubbleSortArraySorter().sort(array1, direction);
        System.out.println("Sort result: " + Arrays.toString(array1));

        Person[] array2 = new RandomPersonArray(5).createRandomArray();
        new BubbleSortArraySorter().sort(array2, new PersonAgeThenNameComparator(), direction);
        System.out.println("Sort by age than name result: " + Arrays.toString(array2));

        Person[] array3 = new RandomPersonArray(5).createRandomArray();
        new BubbleSortArraySorter().sort(array3, new PersonNameThenAgeComparator(), direction);
        System.out.println("Sort by name than age result: " + Arrays.toString(array3));

        Person[] array4 = new RandomPersonArray(5).createRandomArray();
        new InsertionSortArraySorter().sort(array4, direction);
        System.out.println("Sort result: " + Arrays.toString(array4));

        Person[] array5 = new RandomPersonArray(5).createRandomArray();
        new InsertionSortArraySorter().sort(array5, new PersonAgeThenNameComparator(), direction);
        System.out.println("Sort by age than name result: " + Arrays.toString(array5));

        Person[] array6 = new RandomPersonArray(5).createRandomArray();
        new InsertionSortArraySorter().sort(array6, new PersonNameThenAgeComparator(), direction);
        System.out.println("Sort by name than age result: " + Arrays.toString(array6));
    }
}
