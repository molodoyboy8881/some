package com.geekhub.hw5.task2.sorters;

import com.geekhub.hw5.task2.direction.Direction;

import java.util.Comparator;

public class InsertionSortArraySorter implements ArraySorter {
    @Override
    public <T extends Comparable<T>> void sort(T[] array, Direction direction) {
        for (int i = 0; i < array.length; ++i) {
            T value = array[i];
            int j = i - 1;
            for (; j >= 0; --j) {
                if (chooseDirection(array[j].compareTo(value), direction)) {
                    array[j + 1] = array[j];
                } else {
                    break;
                }
            }
            array[j + 1] = value;
        }
    }

    @Override
    public <T> void sort(T[] array, Comparator<T> comparator, Direction direction) {
        for (int i = 0; i < array.length; ++i) {
            T value = array[i];
            int j = i - 1;
            for (; j >= 0; --j) {
                if (chooseDirection(comparator.compare(array[j], value), direction)) {
                    array[j + 1] = array[j];
                } else {
                    break;
                }
            }
            array[j + 1] = value;
        }
    }
}
