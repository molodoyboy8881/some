package com.geekhub.hw5.task2.person;

public class Person implements Comparable<Person> {
    private final String name;
    private final int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public int compareTo(Person o) {
        int cmp = this.name.compareTo(o.name);
        if (cmp != 0) {
            return cmp;
        }

        return Integer.compare(this.age, o.age);
    }

    @Override
    public String toString() {
        return "[name = " + name + ", age = " + age + "]";
    }
}
