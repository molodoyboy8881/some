package com.geekhub.hw5.task2.direction;

public enum Direction {
    ASC(1),
    DSC(-1);

    private final int direction;

    Direction(int direction) {
        this.direction = direction;
    }

    public int getDirection() {
        return direction;
    }
}
