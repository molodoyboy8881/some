package com.geekhub.hw5.task1.application;

import com.geekhub.hw5.task1.inventory.Inventory;
import com.geekhub.hw5.task1.products.*;

public class Application {
    public static void main(String[] args) {
        Inventory inventory = new Inventory();
        inventory.addProduct(new Auto(2, 80000, "Mercedes glc", "hatchback", 2010));
        inventory.addProduct(new Auto(1, 40000, "Subaru forester", "crossover", 2016));
        inventory.addProduct(new Sneakers(1, 200, "Jordan Retro 1", 44, "lifestyle"));
        inventory.addProduct(new Sneakers(2, 200, "Jordan Retro 1", 44, "lifestyle"));
        inventory.addProduct(new Telephone(3, 500, "Redmi Note 3", 5, 16));
        inventory.addProduct(new Telephone(1, 500, "Redmi Note 3", 5, 32));
        inventory.addProduct(new Telephone(2, 1000, "Iphone 11", 6, 64));

        System.out.println("Sum of inventory value: " + inventory.sumUpInventoryValue());
        System.out.println(inventory.showInventoryValue());
    }
}
