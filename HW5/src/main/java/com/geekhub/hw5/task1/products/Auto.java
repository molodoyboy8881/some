package com.geekhub.hw5.task1.products;

public class Auto extends Product {
    private final String type;
    private final int year;

    public Auto(int quantity, float price, String name, String type, int year) {
        super(name, quantity, price);
        this.type = type;
        this.year = year;
    }

    public String showInfo() {
        return super.showInfo() + ",type " + type
                + ",year " + year + ".";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        if (!super.equals(o)) {
            return false;
        }

        Auto auto = (Auto) o;
        if (year != auto.year) {
            return false;
        }

        return type.equals(auto.type);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + year;

        return result;
    }
}
