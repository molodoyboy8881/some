package com.geekhub.hw5.task1.products;

public class Product {
    private final String name;
    private int quantity;
    private final float price;

    public Product(String name, int quantity, float price) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public String showInfo() {
        return "Product " + name + ",of price " + price
                + ",quantity " + quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public float getPrice() {
        return price;
    }

    public void increaseQuantity(int quantity) {
        this.quantity += quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Product product = (Product) o;
        if (Float.compare(product.price, price) != 0) {
            return false;
        }

        return name.equals(product.name);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + (price != +0.0f ? Float.floatToIntBits(price) : 0);

        return result;
    }
}
