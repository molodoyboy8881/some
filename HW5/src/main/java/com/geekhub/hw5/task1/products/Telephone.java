package com.geekhub.hw5.task1.products;

public class Telephone extends Product {
    private final float diagonal;
    private final int memory;

    public Telephone(int quantity, float price, String name, float diagonal, int memory) {
        super(name, quantity, price);
        this.diagonal = diagonal;
        this.memory = memory;
    }

    public String showInfo() {
        return super.showInfo() + ",diagonal " + diagonal
                + ",memory " + memory + ".";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        if (!super.equals(o)) {
            return false;
        }

        Telephone telephone = (Telephone) o;
        if (Float.compare(telephone.diagonal, diagonal) != 0) {
            return false;
        }

        return memory == telephone.memory;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (diagonal != +0.0f ? Float.floatToIntBits(diagonal) : 0);
        result = 31 * result + memory;

        return result;
    }
}
