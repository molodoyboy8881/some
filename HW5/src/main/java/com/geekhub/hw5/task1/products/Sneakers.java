package com.geekhub.hw5.task1.products;

public class Sneakers extends Product {
    private final float size;
    private final String type;

    public Sneakers(int quantity, float price, String name, int size, String type) {
        super(name, quantity, price);
        this.size = size;
        this.type = type;
    }

    public String showInfo() {
        return super.showInfo() + ",size " + size
                + ",type " + type + ".";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        if (!super.equals(o)) {
            return false;
        }

        Sneakers sneakers = (Sneakers) o;
        if (Float.compare(sneakers.size, size) != 0) {
            return false;
        }

        return type.equals(sneakers.type);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (size != +0.0f ? Float.floatToIntBits(size) : 0);
        result = 31 * result + type.hashCode();

        return result;
    }
}

