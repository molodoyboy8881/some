package com.geekhub.hw5.task1.inventory;

import com.geekhub.hw5.task1.products.Product;

import java.util.ArrayList;
import java.util.Iterator;

public class Inventory {
    private final ArrayList<Product> products;

    public Inventory() {
        products = new ArrayList<>();
    }

    public void addProduct(Product product) {
        int index = products.indexOf(product);
        if (index != -1) {
            products.get(index).increaseQuantity(product.getQuantity());
        } else {
            products.add(product);
        }
    }

    public long sumUpInventoryValue() {
        long result = 0;
        Iterator<Product> iterator = products.iterator();
        while (iterator.hasNext()) {
            Product product = iterator.next();
            result += product.getPrice() * product.getQuantity();
        }

        return result;
    }

    public String showInventoryValue() {
        StringBuilder result = new StringBuilder();
        Iterator<Product> iterator = products.iterator();
        while (iterator.hasNext()) {
            result.append(iterator.next().showInfo()).append("\n");
        }

        return result.toString();
    }
}
