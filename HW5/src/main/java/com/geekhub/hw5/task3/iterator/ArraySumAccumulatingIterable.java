package com.geekhub.hw5.task3.iterator;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArraySumAccumulatingIterable implements Iterable<Integer> {
    private final int[] array;

    public ArraySumAccumulatingIterable(int[] array) {
        this.array = array;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new IteratorImpl(array);
    }

    private final static class IteratorImpl implements Iterator<Integer> {
        private int sum;
        private int index;
        private final int[] array;

        IteratorImpl(int[] array) {
            this.array = array;
        }

        @Override
        public boolean hasNext() {
            return index < array.length;
        }

        @Override
        public Integer next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            sum += array[index];
            index += 1;

            return sum;
        }
    }
}


