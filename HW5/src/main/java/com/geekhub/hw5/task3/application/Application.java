package com.geekhub.hw5.task3.application;

import com.geekhub.hw5.task3.iterator.ArraySumAccumulatingIterable;
import com.geekhub.hw5.task3.iterator.OnlyOddIterable;

import java.util.Iterator;

public class Application {
    public static void main(String[] args) {
        ArraySumAccumulatingIterable array1 = new ArraySumAccumulatingIterable(new int[]{1, 2, 3, 4, 5});
        Iterator<Integer> iterator1 = array1.iterator();
        while (iterator1.hasNext()) {
            System.out.print(iterator1.next() + " ");
        }

        System.out.println();

        ArraySumAccumulatingIterable array2 = new ArraySumAccumulatingIterable(new int[]{1, 2, 3, 4, 5});
        Iterator<Integer> iterator2 = array2.iterator();
        System.out.print(iterator2.next() + " ");
        System.out.print(iterator2.next() + " ");
        System.out.print(iterator2.next() + " ");
        System.out.print(iterator2.next() + " ");
        System.out.print(iterator2.next() + " ");

        System.out.println();

        OnlyOddIterable array3 = new OnlyOddIterable(new int[]{1, 2, 3, 4, 4, 5});
        for (Integer e : array3) {
            System.out.print(e + " ");
        }

        System.out.println();

        OnlyOddIterable array4 = new OnlyOddIterable(new int[]{1, 2, 3, 4, 4, 5});
        Iterator<Integer> iterator4 = array4.iterator();
        System.out.print(iterator4.next() + " ");
        System.out.print(iterator4.next() + " ");
        System.out.print(iterator4.next() + " ");
    }
}
