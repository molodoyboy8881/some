package com.geekhub.hw5.task3.iterator;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class OnlyOddIterable implements Iterable<Integer> {
    private final int[] array;

    public OnlyOddIterable(int[] array) {
        this.array = array;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new IteratorImpl(array);
    }

    private final static class IteratorImpl implements Iterator<Integer> {
        private int index;
        private final int[] array;

        public IteratorImpl(int[] array) {
            this.array = array;
        }

        @Override
        public boolean hasNext() {
            while (index < array.length) {
                if (array[index] % 2 != 0) {
                    return true;
                }

                index += 1;
            }

            return false;
        }

        @Override
        public Integer next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }

            int result = array[index];
            index += 1;

            return result;
        }
    }
}
