package com.geekhub.hw3.task1.activities;

import com.geekhub.hw3.task1.converters.CurrencyConverter;
import com.geekhub.hw3.task1.values.Currency;

import java.time.LocalDate;

public class Deposit implements CurrencyConverter {
    private final Currency currency;
    private LocalDate currentDate;
    private float moneyAmount;
    private final float yearPercent;

    public Deposit(Currency currency, LocalDate startDate, float moneyAmount,
                   float yearPercent) {
        this.currency = currency;
        this.currentDate = startDate;
        this.moneyAmount = moneyAmount;
        this.yearPercent = yearPercent;
    }

    public float updateDeposit(LocalDate date) {
        currentDate = date.minusYears(currentDate.getYear());
        currentDate = date.minusMonths(currentDate.getMonthValue());
        currentDate = date.minusDays(currentDate.getDayOfMonth());

        moneyAmount = moneyAmount + (currentDate.getYear() + currentDate.getMonthValue() / 12.f)
                * moneyAmount * yearPercent * 0.01f;
        this.currentDate = date;

        return moneyAmount;
    }

    @Override
    public float convertCurrency(Currency inWhatCurrency) {
        return moneyAmount / currency.getRate() * inWhatCurrency.getRate();
    }

    public float getMoneyAmount() {
        return moneyAmount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public String showInfo() {
        return "On deposit there is " + moneyAmount + " in currency " + currency
                + " for " + yearPercent + " percent per year on current date " + currentDate + ".";
    }
}
