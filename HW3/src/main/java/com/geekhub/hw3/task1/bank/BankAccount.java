package com.geekhub.hw3.task1.bank;

import com.geekhub.hw3.task1.activities.Deposit;
import com.geekhub.hw3.task1.activities.DollarAccount;
import com.geekhub.hw3.task1.activities.PreciousMetalsStorage;
import com.geekhub.hw3.task1.customers.Customer;
import com.geekhub.hw3.task1.customers.LegalEntity;
import com.geekhub.hw3.task1.values.Currency;

import java.util.ArrayList;

public class BankAccount {
    private final Customer customer;
    private final ArrayList<Deposit> deposits;
    private final ArrayList<DollarAccount> dollarAccounts;
    private final ArrayList<PreciousMetalsStorage> storages;

    BankAccount(Customer customer) {
        this.customer = customer;
        this.deposits = new ArrayList<>();
        this.dollarAccounts = new ArrayList<>();
        this.storages = new ArrayList<>();
    }

    public boolean makeDeposit(Deposit deposit) {
        if (deposit == null) {
            return false;
        }

        if (customer instanceof LegalEntity) {
            for (DollarAccount account : dollarAccounts) {
                if (account.getMoneyAmount() >= deposit.convertCurrency(Currency.DOLLAR)) {
                    account.takeMoneyFromAccount(deposit.convertCurrency(Currency.DOLLAR));
                    deposits.add(deposit);
                    return true;
                }
            }
        }

        if (deposit.getMoneyAmount() > 0) {
            deposits.add(deposit);
            return true;
        }

        return false;
    }

    public boolean openDollarAccount(DollarAccount account) {
        if (account == null) {
            return false;
        }

        if (customer instanceof LegalEntity) {
            LegalEntity legalEntity = (LegalEntity) customer;
            if (!legalEntity.isCounterpartyExists()) {
                return false;
            }
        }

        if (account.getMoneyAmount() > 0) {
            dollarAccounts.add(account);
            return true;
        }

        return false;
    }

    public boolean openPreciousMetalsStorage(PreciousMetalsStorage storage) {
        if (storage == null) {
            return false;
        }

        if (storage.getKilograms() > 0) {
            storages.add(storage);
            return true;
        }

        return false;
    }

    public Customer getCustomer() {
        return customer;
    }

    public ArrayList<Deposit> getDeposits() {
        return deposits;
    }

    public ArrayList<DollarAccount> getDollarAccounts() {
        return dollarAccounts;
    }

    public ArrayList<PreciousMetalsStorage> getStorages() {
        return storages;
    }
}