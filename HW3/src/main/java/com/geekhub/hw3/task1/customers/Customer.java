package com.geekhub.hw3.task1.customers;

public interface Customer {
    String getCustomerInfo();
}