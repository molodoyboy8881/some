package com.geekhub.hw3.task1.customers;

public class LegalEntity implements Customer {
    private final String nameOfCompany;
    private final String nameOFOwner;
    private final String patronymicOfOwner;
    private final String surnameOfOwner;
    private final boolean existenceOfCounterparty;

    public LegalEntity(String nameOfCompany, String nameOFOwner, String patronymicOfOwner,
                       String surnameOfOwner, boolean existenceOfCounterparty) {
        this.nameOfCompany = nameOfCompany;
        this.nameOFOwner = nameOFOwner;
        this.patronymicOfOwner = patronymicOfOwner;
        this.surnameOfOwner = surnameOfOwner;
        this.existenceOfCounterparty = existenceOfCounterparty;
    }

    @Override
    public String getCustomerInfo() {
        return "Customers info(type: legal entity, name of company: " + nameOfCompany + ", Full name: "
                + surnameOfOwner + " " + nameOFOwner + " " + patronymicOfOwner + ")";
    }

    public boolean isCounterpartyExists() {
        return existenceOfCounterparty;
    }
}