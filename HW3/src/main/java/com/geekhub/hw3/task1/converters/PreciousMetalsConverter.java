package com.geekhub.hw3.task1.converters;

import com.geekhub.hw3.task1.values.Currency;

public interface PreciousMetalsConverter {
    float convertMetalIntoCurrency(Currency inWhatCurrency);
}
