package com.geekhub.hw3.task1.application;

import com.geekhub.hw3.task1.activities.Deposit;
import com.geekhub.hw3.task1.activities.DollarAccount;
import com.geekhub.hw3.task1.activities.PreciousMetalsStorage;
import com.geekhub.hw3.task1.bank.Bank;
import com.geekhub.hw3.task1.customers.Customer;
import com.geekhub.hw3.task1.customers.Individual;
import com.geekhub.hw3.task1.customers.LegalEntity;
import com.geekhub.hw3.task1.values.Currency;
import com.geekhub.hw3.task1.values.PreciousMetal;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        Bank bank = new Bank();

        Customer person = new Individual("John", "Gates", "Billow");
        Deposit deposit = new Deposit(Currency.HRYVNIA, LocalDate.of(2019, 11, 6),
                10000, 14);
        DollarAccount account = new DollarAccount(20000);
        PreciousMetalsStorage storage = new PreciousMetalsStorage(PreciousMetal.GOLD, 20);

        int id = bank.addCustomer(person);
        bank.addCustomersActivities(id, deposit, account, storage);

        storage = new PreciousMetalsStorage(PreciousMetal.SILVER, 19);
        bank.addCustomersActivities(id, null, null, storage);

        System.out.println(bank.viewCustomersInfo(id));
        System.out.println(bank.viewCustomersActivities(id));

        System.out.println("=======================================");

        Customer legalEntity1 = new LegalEntity("Amazon", "Jeff",
                "Jorgensen", "Bezos", false);
        deposit = new Deposit(Currency.DOLLAR, LocalDate.of(2012, 9, 21),
                5000, 3);
        account = new DollarAccount(1000);
        storage = new PreciousMetalsStorage(PreciousMetal.GOLD, 11);

        id = bank.addCustomer(legalEntity1);
        bank.addCustomersActivities(id, deposit, account, storage);

        System.out.println(bank.viewCustomersInfo(id));
        System.out.println(bank.viewCustomersActivities(id));

        System.out.println("=======================================");

        Customer legalEntity2 = new LegalEntity("Google", "Sergey",
                "Mihalovich", "Brin", true);
        deposit = new Deposit(Currency.RUBEL, LocalDate.of(2016, 1, 10),
                5000, 20);
        account = new DollarAccount(10);
        storage = new PreciousMetalsStorage(PreciousMetal.SILVER, 20);

        id = bank.addCustomer(legalEntity2);
        bank.addCustomersActivities(id, deposit, account, storage);

        System.out.println(bank.viewCustomersInfo(id));
        System.out.println(bank.viewCustomersActivities(id));

        System.out.println("=======================================");

        Customer legalEntity3 = new LegalEntity("SPD", "Bogdan", "Sergiev",
                "Halapin", true);
        deposit = new Deposit(Currency.EURO, LocalDate.of(2012, 2, 11),
                3000, 2);
        account = new DollarAccount(1000);
        storage = new PreciousMetalsStorage(PreciousMetal.GOLD, 20);

        id = bank.addCustomer(legalEntity3);
        bank.addCustomersActivities(id, deposit, account, storage);

        System.out.println(bank.viewCustomersInfo(id));
        System.out.println(bank.viewCustomersActivities(id));

        account = new DollarAccount(10000);

        bank.addCustomersActivities(id, deposit, account, null);
        System.out.println(bank.viewCustomersActivities(id));

        System.out.println("=======================================");

        System.out.println("Price of whole bank activities: ");
        System.out.print(bank.calculateWholePrice(Currency.EURO));
    }
}
