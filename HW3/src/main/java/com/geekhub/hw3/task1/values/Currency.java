package com.geekhub.hw3.task1.values;

public enum Currency {
    EURO(0.86f),
    DOLLAR(1),
    RUBEL(78.96f),
    HRYVNIA(28.43f);

    Currency(float rate) {
        this.rate = rate;
    }

    private final float rate;

    public float getRate() {
        return rate;
    }
}
