package com.geekhub.hw3.task1.bank;

import com.geekhub.hw3.task1.activities.Deposit;
import com.geekhub.hw3.task1.activities.DollarAccount;
import com.geekhub.hw3.task1.activities.PreciousMetalsStorage;
import com.geekhub.hw3.task1.customers.Customer;
import com.geekhub.hw3.task1.values.Currency;

import java.time.LocalDate;
import java.util.HashMap;

public class Bank {
    private int idInit;
    private final LocalDate currentDate;
    private final HashMap<Integer, BankAccount> bankAccounts;

    public Bank() {
        bankAccounts = new HashMap<>();
        currentDate = LocalDate.now();
        idInit = 1000;
    }

    public int addCustomer(Customer customer) {
        BankAccount bankAccount = new BankAccount(customer);
        if (bankAccounts.containsValue(bankAccount) || customer == null) {
            return 0;
        }

        int id = ++idInit;
        bankAccounts.put(id, bankAccount);

        return id;
    }

    public String viewCustomersInfo(int id) {
        BankAccount bankAccount = bankAccounts.get(id);
        if (bankAccount == null) {
            return "Customer does not exist!";
        }

        return bankAccount.getCustomer().getCustomerInfo();
    }

    public boolean addCustomersActivities(int id, Deposit deposit, DollarAccount account,
                                          PreciousMetalsStorage storage) {
        BankAccount bankAccount = bankAccounts.get(id);
        if (bankAccount == null) {
            return false;
        }

        return bankAccount.openDollarAccount(account) | bankAccount.makeDeposit(deposit)
                | bankAccount.openPreciousMetalsStorage(storage);
    }

    public String viewCustomersActivities(int id) {
        BankAccount bankAccount = bankAccounts.get(id);
        if (bankAccount == null) {
            return "Customer does not exist!";
        }

        StringBuilder result = new StringBuilder();

        result.append("Customer's deposits:");
        for (Deposit deposit : bankAccount.getDeposits()) {
            deposit.updateDeposit(currentDate);
            result.append(" ").append(deposit.showInfo());
        }

        result.append("\nCustomer's dollar accounts:");
        for (DollarAccount account : bankAccount.getDollarAccounts()) {
            result.append(" ").append(account.showInfo());
        }

        result.append("\nCustomer's  precious metals storages:");
        for (PreciousMetalsStorage storage : bankAccount.getStorages()) {
            result.append(" ").append(storage.showInfo());
        }

        return result.toString();
    }

    public double calculateWholePrice(Currency inWhatCurrency) {
        double result = 0;

        for (BankAccount bankAccount : bankAccounts.values()) {
            double intermediateResult = 0;

            for (Deposit deposit : bankAccount.getDeposits()) {
                if (inWhatCurrency == deposit.getCurrency()) {
                    intermediateResult += deposit.updateDeposit(currentDate);
                } else {
                    intermediateResult += deposit.convertCurrency(inWhatCurrency);
                }
            }

            for (DollarAccount account : bankAccount.getDollarAccounts()) {
                if (inWhatCurrency != Currency.DOLLAR) {
                    intermediateResult += account.convertCurrency(inWhatCurrency);
                } else {
                    intermediateResult += account.getMoneyAmount();
                }
            }

            for (PreciousMetalsStorage storage : bankAccount.getStorages()) {
                intermediateResult += storage.convertMetalIntoCurrency(inWhatCurrency);
            }

            result += intermediateResult;
        }

        return result;
    }
}
