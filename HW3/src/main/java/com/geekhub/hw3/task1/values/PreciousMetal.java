package com.geekhub.hw3.task1.values;

public enum PreciousMetal {
    GOLD(61322.29f),
    SILVER(763.89f);

    PreciousMetal(float priceFofKg) {
        this.priceFofKg = priceFofKg;
    }

    private final float priceFofKg;

    public float getPriceFofKgInDollars() {
        return priceFofKg;
    }
}
