package com.geekhub.hw3.task1.activities;

import com.geekhub.hw3.task1.converters.CurrencyConverter;
import com.geekhub.hw3.task1.values.Currency;

public class DollarAccount implements CurrencyConverter {
    private float moneyAmount;

    public DollarAccount(float moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    @Override
    public float convertCurrency(Currency inWhatCurrency) {
        return moneyAmount * inWhatCurrency.getRate();
    }

    public float getMoneyAmount() {
        return moneyAmount;
    }

    public void takeMoneyFromAccount(float moneyAmount) {
        this.moneyAmount = this.moneyAmount - moneyAmount;
    }

    public String showInfo() {
        return "On bank account there is " + moneyAmount + " dollars.";
    }
}
