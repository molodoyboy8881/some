package com.geekhub.hw3.task1.customers;

public class Individual implements Customer {
    private final String name;
    private final String patronymic;
    private final String surName;

    public Individual(String name, String patronymic, String surName) {
        this.name = name;
        this.patronymic = patronymic;
        this.surName = surName;
    }

    @Override
    public String getCustomerInfo() {
        return "Customers info(type: individual, Full name: " + surName + " "
                + name + " " + patronymic + ")";
    }
}
