package com.geekhub.hw3.task1.activities;

import com.geekhub.hw3.task1.converters.PreciousMetalsConverter;
import com.geekhub.hw3.task1.values.Currency;
import com.geekhub.hw3.task1.values.PreciousMetal;

public class PreciousMetalsStorage implements PreciousMetalsConverter {
    private final PreciousMetal metalType;
    private final float kilograms;

    public PreciousMetalsStorage(PreciousMetal metalType, float kilograms) {
        this.metalType = metalType;
        this.kilograms = kilograms;
    }

    @Override
    public float convertMetalIntoCurrency(Currency inWhatCurrency) {
        return metalType.getPriceFofKgInDollars() * kilograms * inWhatCurrency.getRate();
    }

    public String showInfo() {
        return "In storage there are " + kilograms + " kilograms"
                + " of " + metalType + ".";
    }

    public float getKilograms() {
        return kilograms;
    }
}
