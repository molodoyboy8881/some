package com.geekhub.hw3.task2.application;

import com.geekhub.hw3.task2.abbreviation.WordsAbbreviation;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.print("Enter number of words: ");

        Scanner scanner = new Scanner(System.in);
        int numberOfWords = Integer.parseInt(scanner.nextLine());

        String[] words = new String[numberOfWords];
        for (int i = 0; i < numberOfWords; ++i) {
            System.out.print("Enter " + (i + 1) + "'st word: ");
            words[i] = scanner.nextLine();
        }

        for (int i = 0; i < numberOfWords; ++i) {
            if (words[i].length() > 10) {
                WordsAbbreviation wordsAbbreviation = new WordsAbbreviation(words[i]);
                words[i] = wordsAbbreviation.makeWordAbbreviation();
            }

            System.out.println(words[i]);
        }
    }
}
