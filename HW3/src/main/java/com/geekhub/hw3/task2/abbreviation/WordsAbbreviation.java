package com.geekhub.hw3.task2.abbreviation;

public class WordsAbbreviation {
    private final String longWord;

    public WordsAbbreviation(String longWord) {
        this.longWord = longWord;
    }

    public String makeWordAbbreviation() {
        char firstChar = longWord.charAt(0);
        char lastChar = longWord.charAt(longWord.length() - 1);

        return firstChar + "" + (longWord.length() - 2) + "" + lastChar;
    }
}
